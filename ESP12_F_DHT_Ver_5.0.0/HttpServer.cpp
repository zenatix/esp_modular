/*
 * http_server.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */


#include "config.h"
#include "ZtBsp.h"
#include "HtmlPage.h"
#include "HttpServer.h"
#include "BspEeprom.h"
#include "TempSensor.h"
#include <stdint.h>

ESP8266WebServer server(httpPort_server);		// server at this port with IP 192.168.4.1
extern struct EEP_Strings EEP_String;
extern struct strms counter;
extern struct temp_val temperature;
extern uint32_t response_count;

bool start_access_point(const char *name,const char *password, int mode){
	return WiFi.softAP(name,password,wifi_ap_channel,mode); // Start hidden access point with "ESP_'(mac of device)'" string
}

void http_server_init(void){
	server.on("/", handleRoot);									// set the http server at root
	server.begin();
}

void handle_client(void){
	server.handleClient();					// handle the client at server 192.168.4.1
}

void handleRoot(void) {
#if debug
	for(char i=0;i<server.args();i++){
		Serial.println(server.arg(i));
	}
#endif
	if(server.arg(0)==Page_password){											// If user knows the access password i.e. zenatix123!@#
		//if(server.arg(1)!="") strcpy(EEP_String.AP_password,server.arg(1).c_str()); // If Access password is also filled by user
		strcpy(EEP_String.Network_SSID,server.arg(1).c_str());					// Copy SSID
		strcpy(EEP_String.Network_password,server.arg(2).c_str());				// and password
		strcpy(EEP_String.Network_gateway,server.arg(3).c_str());					// and gateway IP
		//if(server.arg(5)!="") strcpy(EEP_String.AUTH_pass,server.arg(5).c_str()); // If user wants to change the access password too
		if(server.arg(4)=="yes") EEP_String.hide_ssid = 1;						// If ssid has to be hidden
		else if(server.arg(4)=="no") EEP_String.hide_ssid = 0;					// And if not
		eeprom_write_obj(EEP_network_parm_start,(char*)&EEP_String,sizeof(struct EEP_Strings));	// write all the objects to the eeprom
    system_restart_hard();
	}
	   int size = strlen(EEP_String.Network_password);
	   char pass_to_show[size];
	   bool toggle=1;int i=0;
	   for(;i<size;i++){
	       if(toggle)pass_to_show[i] = EEP_String.Network_password[i];
	       else pass_to_show[i] = '*';
	       toggle = !toggle;
	   }
	   pass_to_show[i] = 0;

     if(WiFi.localIP().toString()== "0.0.0.0"){
        	server.send(200, "text/html",page_str1+WiFi.macAddress()+page_str2+WiFi.localIP().toString()+page_str3+wifi_station_get_rssi()  	//And send the html form
        		                +page_str3_5+hrdwre_ver+page_str3_6+EEP_String.firm_ver
        						+page_str4+String(counter.reboot)+page_str5+String(counter.reconnect)+page_str6
        		                +String(response_count++)+page_str7+String(counter.temp_sampling)+page_str7_6+sensor+page_str7_8+String(temperature.temp_val/100.0)+page_str7_8_1 + EEP_String.Network_SSID +page_str7_8_2+ EEP_String.Network_password + page_str7_8_3+EEP_String.Network_gateway+ page_str8+EEP_String.AUTH_pass
        		                +page_str10+EEP_String.Network_SSID+page_str11+pass_to_show
        		                +page_str12+EEP_String.Network_gateway
        						+page_str14+button + page_str15);
     }
     else
     {
      server.send(200, "text/html",page_str1+WiFi.macAddress()+page_str2+WiFi.localIP().toString()+page_str3+wifi_station_get_rssi()    //And send the html form
                    +page_str3_5+hrdwre_ver+page_str3_6+EEP_String.firm_ver
            +page_str4+String(counter.reboot)+page_str5+String(counter.reconnect)+page_str6
                    +String(response_count++)+page_str7+String(counter.temp_sampling)+page_str7_6+sensor+page_str7_8+String(temperature.temp_val/100.0)+page_str7_8_1 + EEP_String.Network_SSID+page_str7_8_2+ EEP_String.Network_password + page_str7_8_3+EEP_String.Network_gateway+page_str8+EEP_String.AUTH_pass
                    +page_str10+EEP_String.Network_SSID+page_str11+pass_to_show
                    +page_str12+EEP_String.Network_gateway
            +page_str14+button + page_str16);
     }
}

