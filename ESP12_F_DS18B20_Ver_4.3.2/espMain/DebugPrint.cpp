/*
 * print.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */


#include "config.h"
#include "ZtBsp.h"
#include "BspEeprom.h"
#include "DebugPrint.h"

extern struct EEP_Strings EEP_String;
extern struct strms counter;

/**
 * @brief print current settings of ESP
 * 
 * @details
 * The function prints following settings on serial paramters
 *          -#  Mac address
 *          -#  Firmware version
 *          -#  passowrd of AP created by ESP.
 *          -#  IP address alloted to the ESP.
 *          -#  Network ssid of AP to which ESP has been provisioned.
 *          -#  Password of AP to which ESP has been provisioned.
 *          -#  Gateway IP
 *          -#  passowrd of AP created by ESP.
 *          -#  Authentication password for web interface created to provision ESP.
 *          -#  Counter of Network failure.
 *          -#  Reboot count.
 *          -#  Reconnect count.
 *          -#  Counter of Temperature failure
 *          -#  Temperature sampling rate
 */
void print_settings(void){
		Serial.println("MAC "+WiFi.macAddress());
		IPAddress myIP = WiFi.softAPIP();
		Serial.println("Firmware Version - "+String(EEP_String.firm_ver));
		Serial.println(EEP_String.AP_password);
		Serial.print("AP IP address: ");
		Serial.println(myIP);
		Serial.println("SSID "+String(EEP_String.Network_SSID));
		Serial.println("Password "+String(EEP_String.Network_password));
		Serial.println("Gateway IP "+String(EEP_String.Network_gateway));
		Serial.println("AP Pass "+String(EEP_String.AP_password));
		Serial.println("AUTH pass "+String(EEP_String.AUTH_pass));
		Serial.println("Net fail "+String(counter.network_fail));
		Serial.println("Reboot "+String(counter.reboot));
		Serial.println("Reconnect "+String(counter.reconnect));
		Serial.println("Temp fail "+String(counter.temp_fail));
		Serial.println("Temp Sampling"+String(counter.temp_sampling));
}
