/**
 * @file  temp_sensor.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef TEMPSENSOR_H_
#define TEMPSENSOR_H_

struct temp_val{
	int16_t temp_val;							// Temperature value storage
};

struct temp_index{
	uint16_t temp;							// Index of temperature value storage
};

#if RST_TEMP
/**
 * @brief     Reset the temperature sensor
 * 
 * This function resets the Temperature sensor by passing the reset pulse
 */
void temp_reset(void);
#endif

/**
 * @brief     Initialise temperature sensor
 * 
 * Initializes the temperature sensors gpio and defines the objects
 */
void temp_sensor_init(void);

/**
 * @brief     Read temperature from the sensor
 * 
 * This function reads the temperature value from the sensor
 * 
 * @return    temperature value
 */
float temperature_read(void);

#endif /* TEMPSENSOR_H_ */
