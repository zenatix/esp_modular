/*
 * @file  ota.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */
#ifndef OTA_H_
#define OTA_H_

/*
 * @brief This function adds the network to OTA session
 * @param   network     Name of network
 * @param   password    password of the network
 * 
 * @return  boolean status
 */
bool ota_access_pt(const char* network,const char* password);

/*
 * @brief Function responsible for OTA update.
 * 
 * This function sends the GET request for binary file after loading the bin file it restarts the system for
 * 
 * @param     new firmware version
 * @param     gateway ip
 * 
 * @return    ota status
 */
int ota_update(String new_firm_ver, const char *host);

#endif /* OTA_H_ */
