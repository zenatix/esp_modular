/*
 * config.h
 *
 *  Created on: 02-Feb-2017
 *      Author: root
 
 In version 4.2.2 default gateway has been changed from 192.168.0.50 to 192.168.10.100
 In version 4.2.4 call system_restart_hard(); after page setup
 
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/*********************************temperature sensor**********************************************************/
#define			LM35						35		// LM35 sensor selection
#define 		DS18B20						20		// DDS18b20 selection
#define 		TEMP_ATTEMPT				5		// Numbers of attempts in case of error value reading
#define 		TEMP_ERROR					200		// Error value in case of temperature sensor failure
#define			TEMP_OFFSET					0		// This is the offset value need to be subtract from the raw reading
#define 		RST_TEMP					1		// Macro that enables the reset functionality of DS18B20. 0 disables

/*********************************functionality selection macros***********************************************/
#define			debug		            	1   	// Debug option 1 enable 0 disable
#define			SENSOR						DS18B20	// put here the sensor being used for LM35 or DS18B20
#define			SYS_CPU						160		// CPU clock at MHz

#if SENSOR == DS18B20
#define 		hrdwre_ver 	 				"13"	  // !!!!Be careful, this keyword forms the directory path to search for OTA binary
#define 		sensor 						"DS18B20"
#endif

#if SENSOR == LM35
#define			hrdwre_ver					"12"	 // !!!!Be careful, this keyword forms the directory path to search for OTA binary
#define 		sensor 						"LM35"
#endif

/********************************index and address macros****************************************************/
#define			new_EEP_location	 		0				 	// for detecting the fresh ESP chip
#define			EEP_network_parm_start		1		 		 	// location of starting of string
#define 		EEP_strm_parm_start			201		 		 	// location of starting of counters
#define			buffer_size					    1800	 		 	// Buffer can hold up-to 1800 temperature values
#define 		buffer_strt_index 			400		 			// Starting EEPROM location
#define 		buffer_max_index  			(buffer_size*2)+buffer_strt_index	// Maximum index for storing the temperature value
#define 		buffer_indx_location 		398		 			// Location where the value of current temperature index is stored


/********************************Wifi settings macros *******************************************************/
#define			httpPort_client				8080			// data is sent to this port of connected gateway
#define			httpPort_server				80				// Run http server at this port of 192.168.4.1
#define 		wifi_ap_channel				11				// Select this channel in the 2.4 GHz Band
#define 		Page_password				"9362849" // This password allows you to make change in setting at 192.168.4.1
#define			AP_password_default			"9362849123"	// default value of password to connect to the network
#define			AUTH_pass_default			"zenatix123"	// Leave this password. currently not being used
#define			Network_SSID_default		"Zenatix_Testing_ESP"	// default network
#define			Network_gateway_default		"192.168.10.100"// default Internet gateway address
#define			Network_password_default 	"qwerty1234"	// default router password
#define			firm_ver_default		 	"4.3.0"			// default initial firmware

/******************************** default settings***********************************************************/
#define			reboot_init					0				// Initial value of reboot counter
#define			reconnect_init				0				// initial value of reconnect counter
#define			temp_sampling_init			30				// Initial value of temperature sampling interval
#define			network_fail_init  			0				// initial value of network fail counter
#define			last_update_init  			0				// initial value of update counter
#define 		hide_ssid_init				0				// 0 Means un-hide, 1 Hide
#define 		put_req_attempt				3				// This is the maximum attempt made when one packet is sent

/*******************************Hardware GPIO configuration*************************************************/
#define 		ONE_WIRE_BUS 				14				// GPIO of one wire device like ds18b20
#define 		TEMP_RST_PIN				16				// GPIO of temperature reset
#define			BOARD_LED					2		// Blue LED connected on board
#define 		SYS_RST_PIN					12				// ESP self_reset pin

/*****************************OTA Macros********************************************************************/
#define			ota_enable					1							// Enable OTA feature 1 = enable, 0 = disable
#define 		ota_dir_path				"/docs/ESP_OTA_binaries/hardware_v_"	//directory base to query OTA binary
#define 		ota_timeout					60000			// wait for file for this ms. Time out in case of file is not being received

/*****************************Mac spoof Macros********************************************************************/
#define			mac_spoof					0
#if mac_spoof
#define 		mac5						0x60
#define			mac4						0x01
#define			mac3						0x94
#define			mac2						0x0B
#define			mac1						0x5B
#define 		mac0						0xFD
#endif

#endif /* CONFIG_H_ */
