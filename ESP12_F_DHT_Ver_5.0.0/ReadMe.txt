Firmware release document - Firmware version 5.0.0

The new firmware works with DHT22( temperature and humidity sensor ). "DHT.h" standard library has been used.

only temperature data has been planned to buffered currently due to limited size of memory.

ERROR codes:
	Temperature error :   200
	Humidity error : 201
compatible hardware version is 14.

OTA path has been changed as /sources/firmware/ESP_OTA_binaries/hardware_v_14

The ESP firmware version 5.0.0 is supported by Rpi ver 4.4.5 (commit ID : ad7df46) and Models name is DHTLite in RPi configuration file.

