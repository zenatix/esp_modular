/*
 * HttpClient.h
 *
 *  Created on: 04-Feb-2017
 *      Author: root
 */

#ifndef HTTPCLIENT_H_
#define HTTPCLIENT_H_

/*
 * this function connects the ESP in STA mode to the network
 */
wl_status_t connect_to(const char *network_name,const char *password);

/*
 * This function is called after every sampling interval for either putting data into the buffer or send it directly to gateway
 * arguments : put string, gateway character array, flag = (0 failed, 1 success)
 * returns   : server response string
 */
String put_request(String put_req,char *gateway,int *success);

/*
 * This function checks the connection of server running at gateway
 * arguments : host string, port number
 * returns	 : 1 if connected, 0 if not
 */
int check_connection(const char* host, int port);

/*
 * Read the response from server after data send with put request
 * Returns: values received by the server response
 * Arguments: string that has to be process, integer pointer for holding the cycle rate
 */
String get_response(String extr_val,uint16_t *ret_sample);

#endif /* HTTPCLIENT_H_ */
