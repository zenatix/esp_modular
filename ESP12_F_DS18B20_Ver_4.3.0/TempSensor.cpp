/*
 * temp_sensor.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#include "config.h"
#include "ZtBsp.h"


#if SENSOR == DS18B20										// If sensor is ds18b20
OneWire oneWire(ONE_WIRE_BUS);							// Create one wire object
DallasTemperature sensors(&oneWire);					// pass it to the dallas sensor


#if RST_TEMP
void temp_reset(void){
	digitalWrite(TEMP_RST_PIN,LOW);					// write low at the base of transistor
	for(uint8_t i=1;i!=0;i++);					// wait for a while
	digitalWrite(TEMP_RST_PIN,HIGH);				// make it high again
}
#endif
#endif

void temp_sensor_init(void){
#if SENSOR == DS18B20						// DS18b20 is selected
	sensors.begin();						// initialize the sensor
#if RST_TEMP
	pinMode(TEMP_RST_PIN,OUTPUT);				// set pins for reset
	digitalWrite(TEMP_RST_PIN,HIGH);			// write the high value
#endif
#endif
}


/*
 * Reads the value of ds18b20 temperature
 */
float temperature_read(void){
#if SENSOR == DS18B20
		uint8_t temp_err = 0;			// local error counter
		float tempC = 00.00;			// to store the value of current temperature
here:	sensors.requestTemperatures();			// request the temperature sensor for reading
		tempC = sensors.getTempCByIndex(0);			// get the value of temperature connected to the first index
		if((tempC>-55 && tempC<125)&&(tempC!=85)){		// If temperature gets read with within true range
			return tempC;								// and return
		}
		else {
#if RST_TEMP
			temp_reset();					// Else reset the temperature sensor
#endif
			if(temp_err<TEMP_ATTEMPT){		// check if all reading attempt has been made
				temp_err ++;				// increase the value of temperature error
				goto here;					// read again
			}
			else {
				if(tempC == -127)return TEMP_ERROR;	// return this error value
				else return tempC;				// return 85, if this error has been passed
			}
	}
#endif
#if SENSOR == LM35						// If LM35 is selected as temperature sensor
	return ((((float)analogRead((unsigned char)A0))-TEMP_OFFSET)/10.00);				// read analog value and return
#endif
}

