/*
 * BSP.h
 *
 *  Created on: 02-Feb-2017
 *      Author: root
 */

#ifndef ZTBSP_H_
#define ZTBSP_H_

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <Ticker.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266httpUpdate.h>

#if SENSOR == Dht
#include <DHT.h>
#endif

#if SENSOR == DS18B20
#include <OneWire.h>
#include <DallasTemperature.h>
#endif

extern "C" {
#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "cont.h"
}

/*
 * Function that turns on the on chip LED
 * arguments : nothing
 * returns	 : nothing
 */
void system_led_on(void);

/*
 * Function that turns off the on chip LED
 * arguments : nothing
 * returns	 : nothing
 */
void system_led_off(void);

/*
 * Function for waiting for definite millisecond
 * arguments : nothing
 * returns	 : nothing
 */
void wait_ms(unsigned long ms);

/*
 * Function that initializes the hardware modules with system configuration
 */
void system_init(void);

/*
 * Calling this function would restart the hardware from GPIO signal at 12th
 */
void system_restart_hard(void);

#endif /* ZTBSP_H_ */
