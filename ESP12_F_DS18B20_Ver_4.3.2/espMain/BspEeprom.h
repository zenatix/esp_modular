/*
 * BSP_EEPROM.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef BSPEEPROM_H_
#define BSPEEPROM_H_

#include <stdint.h>

/** 
 *  @brief Strings which are being stored in EEPROM
 */
struct EEP_Strings{						
  char  Network_SSID[32];
  char  Network_password[32];
  char  Network_gateway[32];
  char  AUTH_pass[32];
  char	AP_password[32];
  char firm_ver[5];
  int last_update;
  uint8_t hide_ssid;
};

/**
 * @brief Health counters
 */
struct strms{

  /** Reboot counter. */
  uint32_t reboot;

  /** Reconnect counter, it increases if attempts of connection successes. */
  uint32_t reconnect;

  /**  Sampling of temperature at this rate. */
  uint8_t  temp_sampling;

  /** Reboot counter when occur due to network fail. */
  uint32_t network_fail;

  /** Reboot counter when reboot due to temp  fail.  */
  uint32_t temp_fail;
};

/**
 * @brief Initialise EEPROM
 * 
 * This function starts the eeprom and initializes to the default configurations
 */
void eeprom_init(void);

/**
 * @brief         EEPROM read function
 * 
 * It read data from the non volatile memory of ESP
 * 
 * @param         addr        Address of NVMEM from where data will be read
 * @param         ptr         Pointer to the variable in which data will be stored
 * @param         siz         Size of data
 * 
 * @return        void
 */
void eeprom_read_obj(int addr, char * ptr, int siz);

/**
 * @brief         EEPROM write function
 * 
 * It writes data inside the non volatile memory of ESP
 * 
 * @param         addr        Address of NVMEM where data will be written
 * @param         ptr         Pointer to the variable which will be written
 * @param         siz         Size of data
 * 
 * @return        void
 */
void eeprom_write_obj(int addr,char * ptr,int siz);

#endif /* BSPEEPROM_H_ */
