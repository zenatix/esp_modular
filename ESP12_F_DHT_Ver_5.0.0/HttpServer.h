/*
 * http_server.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef HTTPSERVER_H_
#define HTTPSERVER_H_

/*
 * Function that initializes the server
 */
void http_server_init(void);
/*
 * Function that starts the Access Point on ESP
 * arguments : name of the network, set password, hide or un-hide
 */
bool start_access_point(const char *name,const char *password, int mode);

/*
 * This function gets called every time when new client is connected to the 192.168.4.1 for sensor configuration
 */
void handleRoot(void);

/*
 * This function invokes the http server to handle the clients at 192.168.4.1
 */
void handle_client(void);

#endif /* HTTPSERVER_H_ */
