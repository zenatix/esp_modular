/** @file  espMain.ino
 *  @brief Firmware for ESP temperature sensor.
 *  
 *  This code supports mac spoofing please un-comment the line in
 *  the file located in left side (project explorer) with this path
 *  arduino/core/core_esp8266_main.cpp in this file change the value of
 *  mac[]
 *  in user_init.c function line 167 and 168
 *  Also wifi access point is created by this code is by default hidden
 *  Buffer is also supported about 15 hr
 *  Fixed the time compensation in second
 *  improved configuration page
 *  OTA feature support with update counter
 *  hardware version 12 - LM35 sensors, 13 - DS18B20
 */
 
#include "BspEeprom.h"
#include "config.h"
#include "TempSensor.h"
#include "ZtBsp.h"
#include "DebugPrint.h"
#include "HttpServer.h"
#include "HttpClient.h"

#if ota_enable
#include "ota.h"
#endif

/**
 * @Globalvariables:
 * EEP_String:     Instance of EEPROM string type.
 * counter:        Instance of health stream structure.
 * temperature:    This instance holds the current read temperature.
 * temp_indx:      Index of buffer is hold by this variable.
 * response_count: Response count for counting successful communication.
 */
struct EEP_Strings EEP_String; 
struct strms counter;					 
struct temp_val temperature;	 
struct temp_index temp_indx;	 
uint32_t response_count = 0;	 

/**
 * @Globalvariables:
 * tick:                object for creating the timer event
 * seconds:             variable for storing second count
 * queue & wifi_net:    flag for choosing either data to send or store in queue
 * network_error:       network error for each reading attempts
 * temp_error:          temperature error for each reading attempts
 * one_sec_flag:        flag for checking if one second has been passed
 * last_upd_cnt:        This counter holds the last status of OTA session
 */
Ticker tick;								 
int16_t seconds = 0;               			 
bool queue = 0,wifi_net = 0;				
uint8_t network_error = 0;					
int temp_error = 0;							
bool one_sec_flag = 0;						 
uint8_t last_upd_cnt = 0;					 

#if SENSOR == LM35
/** Variable for storing the counter for taking rolling average. */
uint16_t temp_avg_count = 0;

/** Variable for storing the average temperature of sampling delay. */
float temp_avg	=	0;
#endif

/** Array to store SSID NAME after replacement of white space with %20. */
char ssid_formated[64];
void format_ssid(char  *str);


/**
 * @brief Replaces white spaces with "%20".
 * 
 * @details
 * 
 * The ssid is being formated for put requrest as put request has problem in parsing of white spaces comes with SSID NAME.
 * The function stores the formated ssid name in global variable ssid_formated.
 * 
 * @param       str       SSID NAME, that may or may not contain white space.             
 */
void format_ssid(char *str)
{
        int i=0,k=0;
        int len=strlen(str);
        for(i=0;i<len;i++)
        {
            if(str[i]==' ') {
                    ssid_formated[k++] = '%';
                    ssid_formated[k++] = '2';
                    ssid_formated[k++] = '0';
            }else{
                    ssid_formated[k++] = str[i];
            }
        }
        ssid_formated[k] = '\0';
}

/**
 * @brief   Every second call back function
 * 
 * @details
 * Actions:
 *        -# Turn system LED on.
 *        -# Read the AP Gpio.
 *        -# In case of temperature sensor LM35 is selected, indicate that one second has been elapsed.
 *        -# if device has to be reset.
 *        -# Debug prints.
 *        -# Increase the heartbeat seconds.
 *        -# If temperature is read faulty 10 times, save reboot cause to eeprom and reset.
 *        -# If gateway is unreachable 10 times, save reboot cause to eeprom and reset.
 *        -# If main program gets hang somewhere and only callback is alive.
 *        -# If device has to be Reset,Finally write all the values and reset with GPIO.
 */
 
void count_sec(void){
	system_led_on();							/* Action 1 */
	int APGpio = digitalRead(4);  /* Action 2 */ 
#if SENSOR == LM35
	one_sec_flag = 1;							/* Action 3 */ 
#endif
	bool reset = 0;								/* Action 4 */ 
#if debug                       /* Action 5 */
	Serial.println("second="+String(seconds)); 
	Serial.println("Net err="+String(network_error));
	Serial.println("temp err="+String(temp_error));
	Serial.println("bfr ptr="+String(temp_indx.temp));
	Serial.println("AP GPIO="+String(APGpio));
#endif
	seconds++;										/* Action 6 */
	if(temp_error>10){
		counter.temp_fail++;			  /* Action 7 */
		reset = 1;
	}
	else if(network_error>=20){
		counter.network_fail++;			/* Action 8 */
		reset  = 1;
	}
	else if((seconds-counter.temp_sampling)>(2*counter.temp_sampling)){	
		reset  = 1;                 /* Action 9 */
	}
	if(seconds<0) seconds = 0;
	if(reset){									  /* Action 10 */
		eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms)); 
		system_restart_hard();											
	}
	if(APGpio==0) seconds = 0 ;

	system_led_off();
}

/** 
 * @brief     Checks reconnect.
 * 
 * This function checks if reconnect is made and increments the reconnect counter.
 * If gateway is present and last time it was not means this time it got connection as well as ip.
 * Increment the reconnect counter and save it to the eeprom.
 * 
 * @returns   bool true if connection is made due to re-attempt
 * 
 */
 
bool check_reconnect(void){
	if((wifi_net) && (WiFi.status()==WL_CONNECTED)&&(WiFi.localIP().toString()!= "0.0.0.0")){	
#if debug
		Serial.println("Reconnection occurred");
#endif
		counter.reconnect ++;	
		eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));
		return 1;
			}
	return 0;
}

/**
 * @brief       Set up function
 * 
 * This function gets called once after powering on
 * 
 * @details 
 * Actions:
 *          -#  System led on.
 *          -#  wait for 10 second while power becomes stable.
 *          -#  Initialize the core and peripheral.
 *          -#  Initialize the eeprom with default values.
 *          -#  Start the Access Point with this password.
 *          -#  Join this network.
 *          -#  Join this network for OTA.
 *          -#  Print some of the things for debugging.
 *          -#  Print debug information.
 *          -#  Initialize the temperature sensors before reading their velues
 *          -#  Initialize the  server and call back function.
 *          -#  Attach the function count_sec() for every one second.  
 *          -#  Indicate that the initialization has been done.
 */
 
void setup() {
	system_led_on();						        /*  Action 1 */
//	system_phy_set_max_tpw(82);
	pinMode(4,INPUT);
#if mac_spoof
	byte STmac[6] = {mac5,mac4,mac3,mac2,mac1,mac0};
	byte APmac[6] = {(mac5+2),mac4,mac3,mac2,mac1,mac0};
	wifi_set_macaddr(STATION_IF, STmac);
	wifi_set_macaddr(SOFTAP_IF, APmac);
#endif
	wait_ms(10000); 							     /* Action 2 */ 
	system_init();						         /* Action 3 */
	eeprom_init();						         /* Action 4 */ 
	start_access_point(("ESP_"+WiFi.macAddress()).c_str(),AP_password_default,EEP_String.hide_ssid); /* Action 5 */
	connect_to(EEP_String.Network_SSID,EEP_String.Network_password);	/* Action 6 */ 
  format_ssid(EEP_String.Network_SSID);
#if ota_enable == 1
	ota_access_pt(EEP_String.Network_SSID, EEP_String.Network_password); /* Action 7 */ 
#endif
#if debug											      /* Action 8 */
	print_settings();								  /* Action 9 */
#endif
	temp_sensor_init();				        /* Action 10 */
	http_server_init();				        /* Action 11 */ 
	tick.attach(1,count_sec);		      /* Action 12 */
	system_led_off();				          /* Action 13 */
}


void loop() {
  /* If sampling time is elapsed. */
	if((seconds-counter.temp_sampling)>=0){				
		system_led_on();
#if debug
		Serial.print("Status code ");
		Serial.println(WiFi.status());
#endif
    /* Check if host(gateway server) is available. */
		if (!check_connection(EEP_String.Network_gateway, httpPort_client)){
      
      /* If wifi is not connected then raise the flag that data has to be buffered and raise the flag that wifi is not available. */
			if((!WiFi.isConnected())|(WiFi.localIP().toString()== "0.0.0.0")){										
				queue = 1;													 
				wifi_net = 1;												 
        if(WiFi.isConnected())
        {
          Serial.print("WIFI connected but ip allocated is");
          Serial.println(WiFi.localIP().toString());
        }

        /* Terminate current process. */
				WiFi.disconnect();											

        /* Try to connect from begin. */ 
				WiFi.begin(EEP_String.Network_SSID,EEP_String.Network_password);

        /* Increase the counter. */
				network_error++;													
#if debug
				Serial.println("Not connected to the network");	
#endif
			}
			else {
#if debug
				Serial.println("Host Not found");
#endif
        /* Terminate current process. */
				WiFi.disconnect();		

        /* Try to connect from begin. */ 
				WiFi.begin(EEP_String.Network_SSID,EEP_String.Network_password);

        /* Do not raise the network error because wifi network is connected. */
				network_error = 0;

        /* Raise the flag that data has to be buffered.*/
				queue = 1;										
			}
		}
		else {												
		  /* Else the device is connected to the network */
      /* check reconnect */
			check_reconnect();	
      
      /* Initialize the flag with zero. */
			queue = 0;										
			wifi_net = 0;
#if debug
			Serial.println("Got IP "+WiFi.localIP().toString());
#endif
      /* Device is connected to gateway so network error would become zero. */
			network_error = 0;									
		}
		if(EEP_String.last_update) last_upd_cnt++;
#if SENSOR == DS18B20

    /* Get the temp in floating type and save it into integer. */
		temperature.temp_val=temperature_read()*100;			
		if((temperature.temp_val)==(TEMP_ERROR*100)){
			temp_error++;
		}
		else{
			temp_error = 0;
		}
#endif
#if SENSOR == LM35
  /* Get the temp in floating type and save it into integer. */
	temperature.temp_val=temp_avg*100;	
#endif
    /* If buffer is overflow start it again */
		if((temp_indx.temp>=buffer_max_index)||(temp_indx.temp<buffer_strt_index)) {
      /* starting from starting index. */
			temp_indx.temp=buffer_strt_index;				

      /* Save in eeprom. */
			eeprom_write_obj(buffer_indx_location,(char*)&temp_indx,sizeof(struct temp_index));	
		}
    /* If device is not connected to gateway. */
		if(queue){												
      /* Save current temperature value.*/
			eeprom_write_obj(temp_indx.temp,(char*)(&temperature),sizeof(struct temp_val));

      /* Increase this value ny two to point next int address. */
			temp_indx.temp+=2;										

      /* Writing the value of index itself. */
			eeprom_write_obj(buffer_indx_location,(char*)(&temp_indx),sizeof(struct temp_index));
		}
		else{													
    /* send to the server. */
		String firmware_v = "";
		String put_req = "";
			   put_req += "PUT /data/Temperature/?mac="                                                                       // else this mean device is connected to the gateway
					   +WiFi.macAddress()							                                                                            // send the client mac
					   +"&Firmware_ver=" + String(EEP_String.firm_ver).substring(0,5)	                                            // current firmware version
					   +"&Hardware_ver="+hrdwre_ver					                                                                      // hardware version
					   +"&reboot=" + String(counter.reboot)			                                                                  // reboot counter
					   +"&temp_reboot="+String(counter.temp_fail)	                                                                // No of temperature failure cause system reboot
					   +"&reconnect_boot="+String(counter.network_fail)	                                                          // No of network failure cause system reboot
				       +"&reconnect="+String(counter.reconnect)			                                                            // no of successful attempt of connection
					   +"&RSSI="+wifi_station_get_rssi()				                                                                  // Print rssi value
					   +"&SSID="+"\'"+ssid_formated+"\'"		                                                                      // print the network name to which it is connected
					   +"&last_updt="+String(EEP_String.last_update)	                                                            // this counter indicates that the last update attempt was made (0 means no attempts
					   	   	   	   	   	   	   	   	   	   	   	   	   	                                                          // 1 means failure attempt
					   	   	   	   	   	   	   	   	   	   	   	   	   	                                                          // 2 means successful attempts
					   +"&delay="+String(counter.temp_sampling)			                                                              // sample delay
					   +"&temperature=";								                                                                          // string keyword for temperature

	   if(temp_indx.temp<=buffer_strt_index){					                                                                    // If device has no buffer means the index is set to the starting
	   	put_req += String(temperature.temp_val/100.00);				                                                            // send the data directly
#if debug
        Serial.println("sent="+String(temperature.temp_val/100.00));
#endif
       }
	   	if((temp_indx.temp>buffer_strt_index)){
	   	 put_req += (String((float)((int16_t)temperature.temp_val/100.00)));                                              //Append the latest temperature values
		 tick.detach();									                                                                                    // Stop the 1 second counter
		 while((temp_indx.temp>buffer_strt_index)){                                                                         // if the buffer is present sent it to the server one by one from latest to last
		  seconds = 0;										                                                                                  // initialize the counter back to 0
#if debug
		  Serial.println(String(temperature.temp_val/100.00));                                                              //Append the buffered temperature values
#endif
		  temp_indx.temp-=2;							                                                                                  // Since this buffer is LIFO decrease the index by two
		  eeprom_read_obj(temp_indx.temp,(char*)(&temperature),sizeof(struct temp_val));                                    // read the value of temperature from stored buffer
		  put_req += (","+String((float)((int16_t)temperature.temp_val/100.00)));		                                        // send it to the server
   		  }
   	  	  tick.attach(1,count_sec);						                                                                          // Start the timer again
	   	 }

		uint16_t get_cycle_rate;												                                                                   // variable to hold the sampling interval
		int success = 0;
/* Invoke the put_request which sends the stream string to the server and returns the server response. Feed this response to get_response() function to extract the firmware
 * version and cycle rate*/
		firmware_v	= get_response(put_request(put_req,EEP_String.Network_gateway,&success),&get_cycle_rate);	            // Store the delay received from the response
#if debug
		Serial.println(firmware_v);								                                                                        // Print the extracted version for debug purpose
		Serial.println(String(EEP_String.firm_ver));				                                                              // Print the current saved version for debug purpose
#endif
		if(success){						                                                                                          // If all the buffers are sent successfully
			  eeprom_write_obj(buffer_indx_location,(char*)(&temp_indx),sizeof(struct temp_index));
		  }

#if ota_enable
		if((firmware_v != "") && (firmware_v != String(EEP_String.firm_ver))){		                                        // If version number received is not blank and equal
			tick.detach();
			EEP_String.last_update = ota_update(firmware_v,EEP_String.Network_gateway);	                                    // then call the OTA function to update with new firmware
			strcpy(EEP_String.firm_ver,(firmware_v).c_str());							                                                  // copy the version number
			if(EEP_String.last_update==2){												                                                          // If ota session success, update counter for plotting the last OTA attempt
				eeprom_write_obj(EEP_network_parm_start,(char *)(&EEP_String),sizeof(struct EEP_Strings));                    // store the new version
#if debug
				Serial.println("OTA Success :)");			                                                                        // Print serially that OTA has done
#endif
			}
			else if(EEP_String.last_update!=2){				                                                                      // else if OTA was failed
#if debug
				Serial.println("OTA Failed :(");			                                                                        // Print error message
#endif
			}
#if debug
			Serial.println(EEP_String.last_update);			                                                                    // Print debug information
			Serial.println(EEP_String.firm_ver);
			Serial.println(EEP_String.Network_SSID);
			Serial.println(EEP_String.AP_password);
			Serial.println(EEP_String.Network_gateway);
			Serial.println("Going for reboot now");
#endif
			system_restart_hard();				                                                                                  // And restart
		}
#endif
		if ((!isnan(get_cycle_rate)) && get_cycle_rate) response_count++;
			  if((!isnan(get_cycle_rate)) && get_cycle_rate && (get_cycle_rate!=counter.temp_sampling)){	                  // If received delay is integer and non zero
				  counter.temp_sampling = get_cycle_rate;					                                                            // Get the value in ram
				  eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));                              // store it into the eeprom
			  }
		}
		seconds = seconds-counter.temp_sampling;                                                                          //For the compensation of the error in count second
/*If the value of second counter is non integer, less then zero (-ve value), or large*/
		if(isnan(seconds)||(seconds<0)||(seconds>counter.temp_sampling)) seconds = 0;
#if SENSOR==LM35
		temp_avg = 0.0;							                                                                                      // after sending the value of temperature reinitialize these with zero
		temp_avg_count = 0;
#endif
		system_led_off();
	}
#if SENSOR==LM35
	if(one_sec_flag){							                                                                                      // If one second is elapsed
		float temp = temperature_read();				                                                                          // get the temperature value from lm35 sensor
		temp_avg_count++;
		temp_avg = (temp_avg*(temp_avg_count-1)+temp)/temp_avg_count;                                                     // take rolling average
#if debug
		Serial.println("current temp = "+String(temp));
		Serial.println("avg temp = "+String(temp_avg));
#endif
		one_sec_flag = 0;					                                                                                        // ensure that the task for averaging is done
	}
#endif
	if (last_upd_cnt>9){
		EEP_String.last_update = 0;
		eeprom_write_obj(EEP_network_parm_start,(char *)(&EEP_String),sizeof(struct EEP_Strings)); 			                  //Read last values
	}
	handle_client();
}

