/*
 * HttpClient.cpp
 *
 *  Created on: 04-Feb-2017
 *      Author: root
 */
#include "config.h"
#include "ZtBsp.h"
#include "BspEeprom.h"
#include "TempSensor.h"

WiFiClient client;							// client object for communication to the server

wl_status_t connect_to(const char *network_name,const char *password){
	return WiFi.begin(network_name,password); //connect to the gateway
}

int check_connection(const char* host, int port){
	return(client.connect(host, port));		// check if host(gateway server) is available.
}

String put_request(String put_req,char *gateway,int *success){
	  put_req += (" HTTP/1.1\r\n");										// HTTP version header
	  put_req += ("Host: " + (String)gateway+"\r\n");	// IP of the gateway
	  put_req += ("User-Agent: BSK/1.0\r\n");
	  put_req += ("Connection: close\r\n");
	  put_req += ("Content-Type: application/x-www-form-urlencoded\r\n");
	  int chunk_size = 2048;						// Size of each packet
	  int len_str = strlen(put_req.c_str());		// length of put request
	  int chunk_n = len_str/chunk_size;				// Number of exact multiple of the packets
	  int chunk_r = len_str%chunk_size;				// Remain last packet
	  int retry_n = 0;
	  if(chunk_n){									// If size of the put request id grater then chunk size
		  for(int i=0;i<chunk_n;i++){					// send all the exact packets one by one
			while(retry_n<3){
			int snd_siz = client.print(put_req.substring((i*chunk_size),((i+1)*chunk_size)));	// send the one chunk
			Serial.println("send_n="+String(snd_siz));		// Print the size
	  			if(snd_siz==chunk_size){						// If all the characters of data has been send
	  				retry_n = 0;								// No more retry
	  				break;
	  			}
	  			else if(retry_n<put_req_attempt){			    // If not sent
	  				retry_n++;									// retry again while incrementing attempts
	  			}
	  			else {
	  				break;										// If Maximum attempts has been done and still not sent
	  			}
			}
		  }
	  }
	  if((chunk_r)&&(retry_n==0)){									// if there is one remain last packet
		  retry_n = 0;
		  while(retry_n<put_req_attempt){
			  int snd_siz = client.print(put_req.substring(chunk_n*chunk_size,len_str));	// send that packet
			  Serial.println("send_r="+String(snd_siz));	// Print the size of that packet
			  if(snd_siz==chunk_r) {
				  *success=1;						// If the data has been published to the server, mark as read
				  break;
			  }
			  else retry_n++;						// otherwise retry
		  }
	  }
	  client.println();					// Print the \r\n
#if debug
	  Serial.println(chunk_n);							// Multiply factor
	  Serial.println(chunk_r);							// And also remain
	  Serial.println("Req Length="+String(len_str));	// Print the length of the string
	  Serial.println("Put request="+put_req);			// and the request just sent
#endif
	  char ch = 0;							// dummy character for storing received byte from server
	  String ser_response ="";				// Dummy string to store whole response line
	  while (client.connected()) {			// while this device is connected
		while (client.available()) {		// while gateway is available
		    ch = client.read();				// read one character
		    ser_response += ch;				// store one by one in whole dummy string
		   }
		}
	  return ser_response;					// Return the server response
}

String get_response(String extr_val,uint16_t *ret_sample){
	char *start,*end;											// pointer variables to store the
	start = strstr(extr_val.c_str(),"<")+1;						// Starting address of response delay received from server
	end	=	strstr(extr_val.c_str(),">&frmwre_v=");				// ending address of response delay received from server
	for(;start<end;start++){									// from starting index to ending index
		(*ret_sample) = ((*ret_sample)*10)+((int)(*start)-48);	// store the returned value in integer format after
	}
#if debug
	Serial.println(extr_val);				//Print the string from which, we have to extract the firmware version and sample
	Serial.println(*ret_sample);
#endif
	String get_version="";										//Initialize with blank
	start = strstr(extr_val.c_str(),"&frmwre_v=")+10;			// find the version starting char pointer
	end	=	strstr(extr_val.c_str(),"&\"");						// find the version ending char pointer
	for(;start<end;start++){									// store the response between them in string
		get_version+=*start;									// extract the version number from the response string
	}
	return get_version;											// Return the version string
}

