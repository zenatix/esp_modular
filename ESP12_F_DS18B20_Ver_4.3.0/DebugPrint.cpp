/*
 * print.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */


#include "config.h"
#include "ZtBsp.h"
#include "BspEeprom.h"
#include "DebugPrint.h"

extern struct EEP_Strings EEP_String;
extern struct strms counter;

void print_settings(void){
		Serial.println("MAC "+WiFi.macAddress());					// Print some parameter for serially debugging
		IPAddress myIP = WiFi.softAPIP();
		Serial.println("Firmware Version - "+String(EEP_String.firm_ver));	// Print serially, the version of firmware
		Serial.println(EEP_String.AP_password);
		Serial.print("AP IP address: ");
		Serial.println(myIP);
		Serial.println("SSID "+String(EEP_String.Network_SSID));
		Serial.println("Password "+String(EEP_String.Network_password));
		Serial.println("Gateway IP "+String(EEP_String.Network_gateway));
		Serial.println("AP Pass "+String(EEP_String.AP_password));
		Serial.println("AUTH pass "+String(EEP_String.AUTH_pass));
		Serial.println("Net fail "+String(counter.network_fail));
		Serial.println("Reboot "+String(counter.reboot));
		Serial.println("Reconnect "+String(counter.reconnect));
		Serial.println("Temp fail "+String(counter.temp_fail));
		Serial.println("Temp Sampling"+String(counter.temp_sampling));
}
