//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2016-08-15 20:34:12

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Ticker.h>

#include "HtmlPage.h"
void EEPROM_write_obj(int addr,char * ptr,int siz);
void EEPROM_read_obj(int addr, char * ptr, int siz);
void count_sec(void);
void handleRoot(void) ;
void EEPROM_init(void);
void setup() ;
void loop() ;

#include "espMain.ino"

