ESP Based Temperature sensors are the wifi based sensor nodes, used to collect the temperature data throughout any infrastructure. In order to analyse the temperature for BMS or cloud based control, one can use the distributed network of temperature sensors. For the large space, this network can be very complex and will consist more than 10 or 100 temperature sensors. As the number of nodes in this network gets increases, the numbers of connections and wiring also gets complex, so reduces the reliability and modularity results difficulty of installation and maintenance.   

In order to reduce the cost of installation and improve the maintainability, ESP8266 offers wireless solution. The complete platform is based on the ESP8266 wifi modules. This sensor has the capability to communicate with the http server and send the data in the digital format. Each sensor node is configurable for the specific server. Complete unit consist these three hardware components:
Power supply of 5 volts
ESP base board with all the components soldered
DS18B20 or LM35 temperature sensor 

ESP8266 is the wifi module that uses xtensa core as the controller component. It has gpio to interface with the analog devices as well as digital. Currently there are two versions available:
LM35 based version
DS18B20 based version

LM35 is an analog sensor which converts the temperature in the analog raw voltage. Temperature can be calculated by reading this analog voltage. While DS18B20 uses digital one wire communication protocol to send its data to the master. Both the sensors are supported in this design.

The complete hardware setup consist these three components:
GateWay (R-Pi)
ESP based temperature sensors
Wireless network or router  

B. INPUT OUTPUT PIN ASSIGNMENT : There are two versions of ESPs  running in the fields:
A. ESP-01 with 512kb flash
B. ESP-12 with 4 Mb flash 
As  discussed above,ESP 12 supports DS18B20 and LM35 sensors. Over the air update feature is also supported.

ESP 1

Function		-	GPIO Number
Self reset		-	0
Onboard LED		-	1
One wire DS18B20 Data	-	2
Temperature Reset	-	Not supported
LM35 analog input	-	Not supported


ESP 12
Self reset		-	12
Onboard LED		-	2
One wire DS18B20 Data	-	14
Temperature Reset	-	16
LM35 analog Input	-	A0

MODULE DESCRIPTION

On the basis of the functionality provided by the whole sensor module, following modules can be categorized:
A. Temperature Sensor
B. HttpClient
C. HttpServer
D. OTA
E. Buffer (BspEeprom.h)

Temperature Sensor : Esp based temperature sensors can support either LM35 or DS18B20 or both, depends upon the configuration in the conf.h file.
These macros are used for the configuration of these sensors:
#define 			ONE_WIRE_BUS 		2		
#define 			TEMP_RST_PIN		16		
#define				SENSOR			DS18B20
#define				TEMP_ATTEMPT		5		
#define				TEMP_ERROR		200		
#define				TEMP_OFFSET		0		
#define				RST_TEMP		0		
	
 ONE_WIRE_BUS defines the gpio connected with data pin of DS18B20. TEMP_RST_PIN is also the gpio number connected to the base of the transistor for resetting DS18B20 device. SENSOR macro selects the code for either the LM35 or DS18B20. While TEMP_ATTEMPT is the number of attempts used for reading the temperature sensor in one shot. TEMP_ERROR defines the error code for false reading after the maximum attempts of reading. TEMP_OFFSET is the macro which is used to calibrate the analog sensor(LM35). While RST_TEMP is the macro which is defined for enabling or disabling the reset functionality while reading in DS18B20 temperature.

B. HttpClient : This module configures the sensor connection on the wifi network. As shown in the block diagram above all the sensor nodes need a common gateway to communicate to the server. Gateway is the device which is responsible to receive the data from all the nodes and send it to the archiver. In order to communicate with this gateway, a simple client and server module is used. All the gateway runs the http server at their network socket and sensor nodes are configured to work as the client. This module has the configuration for client mode.
#define			httpPort_client
#define			Network_SSID_default			
#define			Network_gateway_default	
#define			Network_password_default 	
#define			firm_ver_default		 	

httpPort_client is the macro which defines the network port. For our application, server is running at the port 8080. Also when new device is created, a default setting is being kept for the restoring purpose. Network_SSID_default is the default string macro which keeps the name of the network as the default one. Network_password_default and the Network_gateway_default are the default string macros for the password of default network and the ip of the server at that network.  

C. HttpServer : For working as a client, esp nodes need to have the server information. The default settings of the gateway are given at the first time configuration. But in order to change these settings all esp nodes run the http server at their specific IP (192.168.4.1). Macros defined below are used for this server configuration.


#define					httpPort_server			
#define 				wifi_ap_channel				
#define 				Page_password
#define					AP_password_default 

httpPort_server is the port at which the server is being run. Default one is 80. wifi_ap_channel is the macro which selects the specific channel in 2.4 GHz frequency band. Currently this is 11. Page_password is the password which allows you to make change to the configuration of the html page hosted by it’s server running at 192.168.4.1 . In addition to that, we need to connect to the Access point created by the ESP node. AP_password_default is the password for that purpose. For more information please read the document “ESP sensors configuration” .    

D. OTA : Ota Upgrade or Over The Air Upgrade is another feature which is only supported on ESP-12 based module due to the limited size of flash in ESP-01. Macro related to this feature is described below:
#define			ota_enable		1				
#define 		ota_dir_path		"/docs/ESP_OTA_binaries/hardware_v_"	
#define 		ota_timeout		60000

ota_enable is the macro that enables the OTA feature in the code. Makin it zero will completely disable this feature. ota_dir_path is the string which forms the query path for the binary file on the server in conjunction with the hardware version parameter. ota_timeout is another important macro that tells the code to wait for the binary file to be received until this milliseconds elapsed. This macro defines the timeout in millisecond for receiving the xyz.bin file.

E. Buffer (BspEeprom.h) : The code is written in such a way that if host (gateway) or network is not available, esp stores the data in it’s eeprom. The macro related to this feature is shown below:
#define			buffer_size			1800	 		 	
#define 		buffer_strt_index 		400		 			
#define 		buffer_max_index  		(buffer_size*2)+buffer_strt_index	
#define 		buffer_indx_location 		398		 			

buffer_size is the macro which tells the numbers of data points to be stored. buffer_strt_index is the starting location of buffered data. while buffer_max_index is calculated with the above information. The address of the pointed value in the buffer memory is stored in the buffer_indx_location. 

HEALTH PARAMETER
All the sensor nodes communicate in the htpp format. The complete format of each PUT request is given below.

"PUT /data/Temperature/?mac=5C:CF:7F:24:31:F1&Firmware_ver=1.1.1&Hardware_ver=13&reboot=814&temp_reboot=0&reconnect_boot=765&reconnect=64&RSSI=-67&SSID=ZT_MAIN_ZTLAB1&last_updt=0&delay=30&temperature=22.87 HTTP/1.1" 200 22 "-" "BSK/1.0"

mac: This is the hardware address of the device.
Firmware_ver: This keyword shows the version of firmware its being ran on. 
Hardware_ver: This keyword is the unique id for the OTA upgrade. Each hardware has its own query for the file identified by this field.
reboot: this is the counter which gets incremented every time when device gets reboot.
reconnect: This counter counts the successful attempts of connection to the wifi router.
temp_reboot: This counter indicates that how many times the controller got restarted due to the error temperature reading.
reconnect_boot: This counter gets incremented every time when self restart occurs due to the wifi signal unavailability. 
RSSI: Rssi value tells the signal strength of the router network to which the RPI and esp is connected to.
SSID: This stream information is the name of network to which ESP device is connected to.
last_updt: This counter increments to 2 when an successful ota operation is done. If the value is 1, means ota attempts was made last time but couldn’t upgraded to the latest. Updated value of this counter resides for only 5 minutes.
delay: Delay is another stream parameter which is required to retain the buffer value. When complete data packet containing more than 1 value is sent to the server, server timestamps all the values on the basis of the delay.
temperature: This field holds the current information of the temperature in the put request. If the sensor node is sending more than 1 value, all the values are comma separated.         
 
LINKS:

Schematic, PCB layout and BOM :
https://bitbucket.org/zenatix/hardware/src/ea89403bc2e5cf4a9c7e860f40a93441f9e15c6f/current%20boards/esp12%20f/espv12.1.0/?at=development




