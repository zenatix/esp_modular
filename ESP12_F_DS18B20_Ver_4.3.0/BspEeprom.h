/*
 * BSP_EEPROM.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef BSPEEPROM_H_
#define BSPEEPROM_H_

#include <stdint.h>

struct EEP_Strings{						// Strings which are being stored
  char  Network_SSID[32];
  char  Network_password[32];
  char  Network_gateway[32];
  char  AUTH_pass[32];
  char	AP_password[32];
  char firm_ver[5];
  int last_update;
  uint8_t hide_ssid;
};

struct strms{								// Other counters
  uint32_t reboot;
  uint32_t reconnect;						// Reconnect counter if attempts of connection successes
  uint8_t  temp_sampling;					// Sampling of temperature at this rate
  uint32_t network_fail;					// reboot counter when occur due to network fail
  uint32_t temp_fail;						// reboot counter when reboot due to temp  fail
};

/*
 * This function starts the eeprom and initializes to the default configurations
 * arguments : None
 * returns : None
 */
void eeprom_init(void);

/*
 * Function for reading the data from eeprom
 * Arguments: address from the data has to be read, data structure pointer and size of that structure
 * returns void
 */
void eeprom_read_obj(int addr, char * ptr, int siz);

/*
 * Function that takes structure and stores that into given location
 * Arguments: address where the data has to be stored, data structure pointer and size of that structure
 * Returns: void
 */
void eeprom_write_obj(int addr,char * ptr,int siz);

#endif /* BSPEEPROM_H_ */
