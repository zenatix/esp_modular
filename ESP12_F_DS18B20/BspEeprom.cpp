/*
 * BSP_EEPROM.c
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */
#include "BspEeprom.h"
#include "config.h"
#include "TempSensor.h"
#include "ZtBsp.h"

extern struct EEP_Strings EEP_String;
extern struct strms counter;
extern struct temp_index temp_indx;

void eeprom_write_obj(int addr,char * ptr,int siz){
  for(int i= addr;i<(siz + addr);i++){
    EEPROM.write(i,*(ptr));
    ptr++;
    EEPROM.commit();
  }
}

void eeprom_read_obj(int addr, char * ptr, int siz){
  for(int i= addr;i<(siz + addr);i++){
    *(ptr) = EEPROM.read(i);
    ptr++;
  }
}

void eeprom_init(void){
	EEPROM.begin(buffer_max_index);				// allocate 4000 byte in eeprom maximum is 4096
	uint8_t new_byte = EEPROM.read(new_EEP_location);  // Look for new EEPROM location to detect if chip is new
	if(new_byte!=0){								// If new ESP chip is detected
			strcpy(EEP_String.AP_password,AP_password_default);     // Copy all the default values to the memory
			strcpy(EEP_String.AUTH_pass,AUTH_pass_default);
			strcpy(EEP_String.Network_SSID,Network_SSID_default);
			strcpy(EEP_String.Network_gateway,Network_gateway_default);
			strcpy(EEP_String.Network_password,Network_password_default);
			strcpy(EEP_String.firm_ver,firm_ver_default);
			counter.reboot 			= reboot_init;
			counter.reconnect 		= reconnect_init;
			counter.temp_sampling	= temp_sampling_init;
			counter.network_fail  	= network_fail_init;
			EEP_String.last_update  = last_update_init;
			EEP_String.hide_ssid	= hide_ssid_init;
			temp_indx.temp 			= buffer_strt_index;
			eeprom_write_obj(EEP_network_parm_start,(char *)(&EEP_String),sizeof(struct EEP_Strings));
			eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));
			eeprom_write_obj(buffer_indx_location,(char *)(&temp_indx),sizeof(struct temp_index));
			EEPROM.write(new_EEP_location,0);
			EEPROM.commit();
		}
		eeprom_read_obj(EEP_network_parm_start,(char *)(&EEP_String),sizeof(struct EEP_Strings)); 			//Read last values
		eeprom_read_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));
		eeprom_read_obj(buffer_indx_location,(char *)(&temp_indx),sizeof(struct temp_index));
		counter.reboot ++;																	//Reboot counter gets increase
		eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));
}

