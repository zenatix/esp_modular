/**
 * @file HttpClient.cpp
 *
 *  Created on: 04-Feb-2017
 *      Author: root
 */
#include "config.h"
#include "ZtBsp.h"
#include "BspEeprom.h"
#include "TempSensor.h"

/** Client object for communication to the server. */
WiFiClient client;							

wl_status_t connect_to(const char *network_name,const char *password){
	return WiFi.begin(network_name,password);
}

int check_connection(const char* host, int port){
	return(client.connect(host, port));	
}

String put_request(String put_req,char *gateway,int *success){
    /*  HTTP version header */
	  put_req += (" HTTP/1.1\r\n");					

    /*  IP of the gateway       */
	  put_req += ("Host: " + (String)gateway+"\r\n");
	  put_req += ("User-Agent: BSK/1.0\r\n");
	  put_req += ("Connection: close\r\n");
	  put_req += ("Content-Type: application/x-www-form-urlencoded\r\n");

    /*  Size of each packet*/
	  int chunk_size = 2048;

    /*  Length of put request. */
	  int len_str = strlen(put_req.c_str());
	  
	  /*  Number of exact multiple of the packets. */
	  int chunk_n = len_str/chunk_size;

    /* Remain last packet*/
	  int chunk_r = len_str%chunk_size;
	  int retry_n = 0;

    /* If size of the put request id grater then chunk size */
	  if(chunk_n){									

      /* Send all the exact packets one by one. */
		  for(int i=0;i<chunk_n;i++){
			while(retry_n<3){

      /* Send the one chunk. */
			int snd_siz = client.print(put_req.substring((i*chunk_size),((i+1)*chunk_size)));
			Serial.println("send_n="+String(snd_siz));
          /* If all the characters of data has been send. */
	  			if(snd_siz==chunk_size){		 
            /* No more retry. */
	  				retry_n = 0;								
	  				break;
	  			}
	  			else if(retry_n<put_req_attempt){
          
            /*retry again while incrementing attempts*/			    
	  				retry_n++;									
	  			}
	  			else {
            /* If Maximum attempts has been done and still not sent. */
	  				break;
	  			}
			}
		  }
	  }
   /* If there is one remain last packet.*/
	  if((chunk_r)&&(retry_n==0)){							
		  retry_n = 0;
		  while(retry_n<put_req_attempt){

        /* send that packet */
			  int snd_siz = client.print(put_req.substring(chunk_n*chunk_size,len_str));
			  Serial.println("send_r="+String(snd_siz));
			  if(snd_siz==chunk_r) {

          /*If the data has been published to the server, mark as read */
				  *success=1;						
				  break;
			  }
			  else retry_n++;						
		  }
	  }
	  client.println();				
#if debug
    /* Print Multiply factor, remain last packet, the length of the string and and the request just sent.*/
	  Serial.println(chunk_n);							 
	  Serial.println(chunk_r);							
	  Serial.println("Req Length="+String(len_str));	 
	  Serial.println("Put request="+put_req);			 
#endif
    /*
     * @Variables
     * ch:            Dummy character for storing received byte from server.
     * ser_response:  Dummy string to store whole response line.
    */
	  char ch = 0;							
	  String ser_response ="";		

    /* While this device is connected. */
	  while (client.connected()) {

    /* while gateway is available. */
		while (client.available()) {

        /* read one character */
		    ch = client.read();			

        /* store one by one in whole dummy string */
		    ser_response += ch;			
		   }
		}
	  return ser_response;					
}

String get_response(String extr_val,uint16_t *ret_sample){

  /* Pointer variables to store the Starting address and ending address of response delay received from server*/
	char *start,*end;											
	start = strstr(extr_val.c_str(),"<")+1;						 
	end	=	strstr(extr_val.c_str(),">&frmwre_v=");

  /* From starting index to ending index store the returned value in integer format. */
	for(;start<end;start++){									
		(*ret_sample) = ((*ret_sample)*10)+((int)(*start)-48);
	}
#if debug
  /* Print the string from which, we have to extract the firmware version and sample.*/
	Serial.println(extr_val);				
	Serial.println(*ret_sample);
#endif

  /* initialise a blank string*/
	String get_version="";										

  /* find the version starting char pointer. */
	start = strstr(extr_val.c_str(),"&frmwre_v=")+10;

  /* Find the version ending char pointer */
	end	=	strstr(extr_val.c_str(),"&\"");			
  /*store the response between them in string and extract the version number from the response string */ 			
	for(;start<end;start++){									
		get_version+=*start;									
	}
	return get_version;						
}

