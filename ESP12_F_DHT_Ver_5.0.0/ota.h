/*
 * ota.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */
#ifndef OTA_H_
#define OTA_H_

/*
 * This function adds the network to OTA session
 * arguments : network name and password
 */
bool ota_access_pt(const char* network,const char* password);

/*
 * This function sends the GET request for binary file after loading the bin file it restarts the system for
 * the next startup
 */
int ota_update(String new_firm_ver, const char *host);

#endif /* OTA_H_ */
