/*
 * @file http_server.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef HTTPSERVER_H_
#define HTTPSERVER_H_

/*
 * @brief   Function that initializes the server
 * 
 * Set the http server at root and start the server.
 */
void http_server_init(void);
/*
 * @brief     Start ESP in AP mode
 * 
 * Function that starts the Access Point on ESP with specific SSID and password.
 * We use "ESP_" followed by its mac for the ssid name of AP.
 * 
 * @param    name     SSID name of Network created by ESP
 * @param    password AP password of the created by ESP
 * @param    mode     Set the mode whether ESP will be hiddedn or not
 * 
 * @return   1 if Access point started, 0 if failed to create network.
 */
bool start_access_point(const char *name,const char *password, int mode);

/*
 * @brief   Handles the sensor configuration / provisioning.  
 * 
 * This function gets called every time when new client is connected to the 192.168.4.1 for sensor configuration
 */
void handleRoot(void);

/*
 * @brief   Invoke the http server
 * 
 * This function invokes the http server to handle the clients at 192.168.4.1
 */
void handle_client(void);

#endif /* HTTPSERVER_H_ */
