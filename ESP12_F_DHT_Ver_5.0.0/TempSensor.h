/*
 * temp_sensor.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef TEMPSENSOR_H_
#define TEMPSENSOR_H_

struct temp_val{
	int16_t temp_val;							// Temperature value storage
};

struct temp_index{
	uint16_t temp;							// Index of temperature value storage
};

#if RST_TEMP
/*
 * this function resets the ds18b20 by passing the reset pulse
 * Arguments : nothing
 * Returns : nothing
 */
void temp_reset(void);
#endif

/*
 * Initializes the temperature sensors gpio and defines the objects
 * Arguments : nothing
 * Returns : nothing
 */
void temp_sensor_init(void);

/*
 * This function reads the temperature value from the sensor
 * Arguments : Nothing
 * Return : Temperature in float
 */
float humidity_read(void);
float temperature_read(void);

#endif /* TEMPSENSOR_H_ */
