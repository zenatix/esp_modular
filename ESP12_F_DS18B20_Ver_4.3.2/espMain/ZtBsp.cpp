/*
 * ZT_BSP.c
 *
 *  Created on: 02-Feb-2017
 *      Author: root
 */
#include "ZtBsp.h"
#include "config.h"


void system_led_on(void){
	pinMode(BOARD_LED,OUTPUT);				// Indication LED
	digitalWrite(BOARD_LED,0);				// Make LED ON
}

void system_led_off(void){
	digitalWrite(BOARD_LED,1);				// Make LED ON
}

void wait_ms(unsigned long ms){
	delay(ms);
}

void system_init(void){

  /*  Set CPU frequency to 160 MHz. */
	system_update_cpu_freq(SYS_CPU);

  /*  Set for Station and Access point both*/
	WiFi.mode(WIFI_AP_STA);			

  /* If debug is on, begin the serial at 115200 baud rate*/
#if debug
	Serial.begin(115200);			
	Serial.println();
#endif
}

void system_restart_hard(void){
	EEPROM.end();

  /* wait for buffer empty before restart */
	delay(300);						
	pinMode(SYS_RST_PIN,OUTPUT);
	digitalWrite(SYS_RST_PIN,LOW);
}

