/*
 * This code supports mac spoofing please un-comment the line in
 * the file located in left side (project explorer) with this path
 * arduino/core/core_esp8266_main.cpp in this file change the value of
 * mac[]
 * in user_init.c function line 167 and 168
 * Also wifi access point is created by this code is by default hidden
 * Buffer is also supported about 15 hr
 * Fixed the time compensation in second
 * improved configuration page
 * OTA feature support with update counter
 * hardware version 12 - LM35 sensors, 13 - DS18B20
 */
#include "BspEeprom.h"
#include "config.h"
#include "TempSensor.h"
#include "ZtBsp.h"
#include "DebugPrint.h"
#include "HttpServer.h"
#include "HttpClient.h"

#if ota_enable
#include "ota.h"
#endif

struct EEP_Strings EEP_String;				// Instance of EEPROM string type
struct strms counter;						// Instance of health stream structure
struct temp_val temperature;				// This instance holds the current read temperature
struct temp_index temp_indx;				// Index of buffer is hold by this variable
uint32_t response_count = 0;				// response count for counting successful communication

Ticker tick;								// object for creating the timer event
int16_t seconds = 0;               			// variable for storing second count
bool queue = 0,wifi_net = 0;				// flag for choosing either data to send or store in queue
uint8_t network_error = 0;					// network error for each reading attempts
int temp_error = 0;							// temperature error for each reading attempts
bool one_sec_flag = 0;						// flag for checking if one second has been passed
uint8_t last_upd_cnt = 0;					// This counter holds the last status of OTA session

#if SENSOR == LM35
uint16_t temp_avg_count = 0;				// variable for storing the counter for taking rolling average
float temp_avg	=	0;						// variable for storing the average temperature of sampling delay
#endif


/*
 * Every second call back function
 */
void count_sec(void){
	system_led_on();							// Turn system LED on
#if SENSOR == LM35								// In case of temperature sensor LM35 is selected
	one_sec_flag = 1;							// Indicate that one second has been elapsed
#endif
	bool reset = 0;								// if device has to be reset
#if debug
	Serial.println("second="+String(seconds)); 			// Print the value of seconds for debugging
	Serial.println("Net err="+String(network_error));	// Print the value of network error for debugging
	Serial.println("temp err="+String(temp_error));		// Print the value of temperature error for debugging
	Serial.println("bfr ptr="+String(temp_indx.temp));	// Print the value of buffer index for debugging
#endif
	seconds++;										// Increase the temperature value
	if(temp_error>10){
		counter.temp_fail++;						// If temperature is read faulty 10 times, save reboot cause to eeprom and reset
		reset = 1;
	}
	else if(network_error>=20){
		counter.network_fail++;						// If gateway is unreachable 10 times, save reboot cause to eeprom and reset
		reset  = 1;
	}
	else if((seconds-counter.temp_sampling)>(2*counter.temp_sampling)){	// If main program gets hang somewhere and only callback is alive
		reset  = 1;
	}
	if(seconds<0) seconds = 0;
	if(reset){									// If device has to be Reset
		eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));  // Finally write all the values and
		system_restart_hard();											// and reset with GPIO
	}
	system_led_off();
}


/*
 * This function checks if reconnect is made and increments the reconnect counter
 * Arguments: Nothing
 * Returns: bool true if connection is made due to re-attempt
 */
bool check_reconnect(void){
	if((wifi_net) && (WiFi.status()==WL_CONNECTED)){	// If gateway is present and last time it was not means this time it got connection
#if debug
		Serial.println("Reconnection occurred");
#endif
		counter.reconnect ++;							// Increment the reconnect counter
		eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms));  // save it to the eeprom and return
		return 1;
			}
	return 0;
}

/*
 * This function gets called once after powering on
 */
void setup() {
	system_led_on();						// system led on
#if mac_spoof
	byte STmac[6] = {mac5,mac4,mac3,mac2,mac1,mac0};
	byte APmac[6] = {(mac5+2),mac4,mac3,mac2,mac1,mac0};
	wifi_set_macaddr(STATION_IF, STmac);
	wifi_set_macaddr(SOFTAP_IF, APmac);
#endif
	wait_ms(5000); 							// wait for 5 second while power becomes stable
	system_init();							// Initialize the core and peripheral
	eeprom_init();							// Initialize the eeprom with default values
	start_access_point(("ESP_"+WiFi.macAddress()).c_str(),AP_password_default,EEP_String.hide_ssid); // Start the Access Point with this password
	connect_to(EEP_String.Network_SSID,EEP_String.Network_password);	// Join this network
#if ota_enable == 1
	ota_access_pt(EEP_String.Network_SSID, EEP_String.Network_password); // Join this network for OTA
#endif
#if debug											// Print some of the things for debugging
	print_settings();								// Print debug information
#endif
	temp_sensor_init();				// Initialize the temperature sensors before reading their velues
	http_server_init();				// initialize the  server and call back function
	tick.attach(1,count_sec);		// Attach the function count_sec() for every one second												// start that server
	system_led_off();				// indicate that the initialization has been done
}


void loop() {
	if((seconds-counter.temp_sampling)>=0){				// If sampling time is elapsed
		system_led_on();
#if debug
		Serial.print("Status code ");
		Serial.println(WiFi.status());					// this error
#endif
		if (!check_connection(EEP_String.Network_gateway, httpPort_client)){		// check if host(gateway server) is available.
			if(!WiFi.isConnected()){										//if not then
				queue = 1;													// raise the flag that data has to be buffered and
				wifi_net = 1;												// raise the flag that wifi is not available
				WiFi.disconnect();											// terminate current process
				WiFi.begin(EEP_String.Network_SSID,EEP_String.Network_password);	// and try to connect from begin
				network_error++;													// increase this counter means
#if debug
				Serial.println("Not connected to the network");						// print this line and
#endif
			}
			else {
#if debug
				Serial.println("Host Not found");				// print this line and
#endif
				WiFi.disconnect();											// terminate current process
				WiFi.begin(EEP_String.Network_SSID,EEP_String.Network_password);	// and try to connect from begin
				network_error = 0;								// Do not raise the network error because wifi network is connected
				queue = 1;										// raise the flag that data has to be buffered
			}
		}
		else {												// Else the device is connected to the network
			check_reconnect();								// check reconnect
			queue = 0;										// initialize the flag with zero
			wifi_net = 0;
#if debug
			Serial.println("Got IP "+WiFi.localIP().toString());
#endif
			network_error = 0;									// else device is connected to gateway so network error would become zero
		}
		if(EEP_String.last_update) last_upd_cnt++;
#if SENSOR == DS18B20
		temperature.temp_val=temperature_read()*100;			// get the temp in floating type and save it into integer
		if((temperature.temp_val)==(TEMP_ERROR*100)){
			temp_error++;
		}
		else{
			temp_error = 0;
		}
#endif
#if SENSOR == LM35
	temperature.temp_val=temp_avg*100;							// get the temp in floating type and save it into integer
#endif
		if((temp_indx.temp>=buffer_max_index)||(temp_indx.temp<buffer_strt_index)) {	// if buffer is overflow start it again
			temp_indx.temp=buffer_strt_index;					// starting from starting index
			eeprom_write_obj(buffer_indx_location,(char*)&temp_indx,sizeof(struct temp_index));	// saving in eeprom
		}
		if(queue){												// If device is not connected to gateway
			eeprom_write_obj(temp_indx.temp,(char*)(&temperature),sizeof(struct temp_val));	//	save current temperature value
			temp_indx.temp+=2;										// increase this value ny two to point next int address
			eeprom_write_obj(buffer_indx_location,(char*)(&temp_indx),sizeof(struct temp_index));	// writing the value of index itself
		}
		else{													// send to the server
		String firmware_v = "";
		String put_req = "";
			   put_req += "PUT /data/Temperature/?mac="         // else this mean device is connected to the gateway
					   +WiFi.macAddress()							// send the client mac
					   +"&Firmware_ver=" + String(EEP_String.firm_ver).substring(0,5)	//current firmware version
					   +"&Hardware_ver="+hrdwre_ver					// hardware version
					   +"&reboot=" + String(counter.reboot)			// reboot counter
					   +"&temp_reboot="+String(counter.temp_fail)	// No of temperature failure cause system reboot
					   +"&reconnect_boot="+String(counter.network_fail)	// No of network failure cause system reboot
				       +"&reconnect="+String(counter.reconnect)			// no of successful attempt of connection
					   +"&RSSI="+wifi_station_get_rssi()				// Print rssi value
					   +"&SSID="+String(EEP_String.Network_SSID)		// print the network name to which it is connected
					   +"&last_updt="+String(EEP_String.last_update)	// this counter indicates that the last update attempt was made (0 means no attempts
					   	   	   	   	   	   	   	   	   	   	   	   	   	// 1 means failure attempt
					   	   	   	   	   	   	   	   	   	   	   	   	   	// 2 means successful attempts
					   +"&delay="+String(counter.temp_sampling)			// sample delay
					   +"&temperature=";								// string keyword for temperature

	   if(temp_indx.temp<=buffer_strt_index){					// If device has no buffer means the index is set to the starting
	   	put_req += String(temperature.temp_val/100.00);				// send the data directly
#if debug
        Serial.println("sent="+String(temperature.temp_val/100.00));
#endif
       }
	   	if((temp_indx.temp>buffer_strt_index)){
	   	 put_req += (String((float)((int16_t)temperature.temp_val/100.00))+","); //Append the latest temperature values
		 tick.detach();									// Stop the 1 second counter
		 while((temp_indx.temp>buffer_strt_index)){        // if the buffer is present sent it to the server one by one from latest to last
		  seconds = 0;										// initialize the counter back to 0
#if debug
		  Serial.println(String(temperature.temp_val/100.00)+","); //Append the buffered temperature values
#endif
		  temp_indx.temp-=2;							// Since this buffer is LIFO decrease the index by two
		  eeprom_read_obj(temp_indx.temp,(char*)(&temperature),sizeof(struct temp_val)); // read the value of temperature from stored buffer
		  put_req += (String((float)((int16_t)temperature.temp_val/100.00))+",");		// send it to the server
   		  }
   	  	  tick.attach(1,count_sec);						// Start the timer again
	   	 }

		uint16_t get_cycle_rate;												// variable to hold the sampling interval
		int success = 0;
/* Invoke the put_request which sends the stream string to the server and returns the server response. Feed this response to get_response() function to extract the firmware
 * version and cycle rate*/
		firmware_v	= get_response(put_request(put_req,EEP_String.Network_gateway,&success),&get_cycle_rate);	// Store the delay received from the response
#if debug
		Serial.println(firmware_v);								// Print the extracted version for debug purpose
		Serial.println(String(EEP_String.firm_ver));				// Print the current saved version for debug purpose
#endif
		if(success){						// If all the buffers are sent successfully
			  eeprom_write_obj(buffer_indx_location,(char*)(&temp_indx),sizeof(struct temp_index));
		  }

#if ota_enable
		if((firmware_v != "") && (firmware_v != String(EEP_String.firm_ver))){		// If version number received is not blank and equal
			tick.detach();
			EEP_String.last_update = ota_update(firmware_v,EEP_String.Network_gateway);	// then call the OTA function to update with new firmware
			strcpy(EEP_String.firm_ver,(firmware_v).c_str());							// copy the version number													// update counter for plotting the last OTA attempt
			if(EEP_String.last_update==2){												// If ota session success
				eeprom_write_obj(EEP_network_parm_start,(char *)(&EEP_String),sizeof(struct EEP_Strings)); // store the new version
#if debug
				Serial.println("OTA Success :)");			// Print serially that OTA has done
#endif
			}
			else if(EEP_String.last_update!=2){				// else if OTA was failed
#if debug
				Serial.println("OTA Failed :(");			// Print error message
#endif
			}
#if debug
			Serial.println(EEP_String.last_update);			// Print debug information
			Serial.println(EEP_String.firm_ver);
			Serial.println(EEP_String.Network_SSID);
			Serial.println(EEP_String.AP_password);
			Serial.println(EEP_String.Network_gateway);
			Serial.println("Going for reboot now");
#endif
			system_restart_hard();				// And restart
		}
#endif
		if ((!isnan(get_cycle_rate)) && get_cycle_rate) response_count++;
			  if((!isnan(get_cycle_rate)) && get_cycle_rate && (get_cycle_rate!=counter.temp_sampling)){	// If received delay is integer and non zero
				  counter.temp_sampling = get_cycle_rate;					// Get the value in ram
				  eeprom_write_obj(EEP_strm_parm_start,(char *)(&counter),sizeof(struct strms)); // store it into the eeprom
			  }
		}
		seconds = seconds-counter.temp_sampling; //For the compensation of the error in count second
/*If the value of second counter is non integer, less then zero (-ve value), or large*/
		if(isnan(seconds)||(seconds<0)||(seconds>counter.temp_sampling)) seconds = 0;
#if SENSOR==LM35
		temp_avg = 0.0;							// after sending the value of temperature reinitialize these with zero
		temp_avg_count = 0;
#endif
		system_led_off();
	}
#if SENSOR==LM35
	if(one_sec_flag){							// If one second is elapsed
		float temp = temperature_read();				// get the temperature value from lm35 sensor
		temp_avg_count++;
		temp_avg = (temp_avg*(temp_avg_count-1)+temp)/temp_avg_count; // take rolling average
#if debug
		Serial.println("current temp = "+String(temp));
		Serial.println("avg temp = "+String(temp_avg));
#endif
		one_sec_flag = 0;					// ensure that the task for averaging is done
	}
#endif
	if (last_upd_cnt>9){
		EEP_String.last_update = 0;
		eeprom_write_obj(EEP_network_parm_start,(char *)(&EEP_String),sizeof(struct EEP_Strings)); 			//Read last values
	}
	handle_client();
}

