/**
 * @file  temp_sensor.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#include "config.h"
#include "ZtBsp.h"

/*  If sensor is ds18b20, Create one wire object and pass it to the dallas sensor. */
#if SENSOR == DS18B20										
OneWire oneWire(ONE_WIRE_BUS);							
DallasTemperature sensors(&oneWire);				

/* if RST_TEMP is true reset the temperature by sending a rising pulse. */
#if RST_TEMP
void temp_reset(void){
	digitalWrite(TEMP_RST_PIN,LOW);					
	for(uint8_t i=1;i!=0;i++);					
	digitalWrite(TEMP_RST_PIN,HIGH);		
}
#endif
#endif

/* if DS18b20 is selected, initialize the sensor, set pins for reset and write the high value */
void temp_sensor_init(void){
#if SENSOR == DS18B20				
	sensors.begin();						
#if RST_TEMP
	pinMode(TEMP_RST_PIN,OUTPUT);				
	digitalWrite(TEMP_RST_PIN,HIGH);		
#endif
#endif
}

/* Reads the value of ds18b20 temperature */
float temperature_read(void){
#if SENSOR == DS18B20
		uint8_t temp_err = 0;			// local error counter
		float tempC = 00.00;			// to store the value of current temperature
here:	sensors.requestTemperatures();			// request the temperature sensor for reading
		tempC = sensors.getTempCByIndex(0);			// get the value of temperature connected to the first index
		if((tempC>-55 && tempC<125)&&(tempC!=85)){		// If temperature gets read with within true range
			return tempC;								// and return
		}
		else {
#if RST_TEMP
			temp_reset();					
#endif
      /* check if all reading attempt has not been made increase the value of temperature error and try to read again. */
			if(temp_err<TEMP_ATTEMPT){		
				temp_err ++;				
				goto here;					
			}
			else {
        /* return this error value. */
				if(tempC == -127)return TEMP_ERROR;	
        /* return 85, if this error has been passed */
				else return tempC;				
			}
	}
#endif

/* If LM35 is selected as temperature sensor. read analog value and return*/
#if SENSOR == LM35						
	return ((((float)analogRead((unsigned char)A0))-TEMP_OFFSET)/10.00);				
#endif
}

