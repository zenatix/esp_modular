/**
 * @file http_server.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 *  Change Log :
 *      All the changes happened in “html.h” and “http_server.cpp” 
 *      Removed :
 *              AP password field
 *              new access password field
 *      Added    :
 *              Network SSID , Password and Gateway Displayed on the page
 *              prefilled Network password , hide ssid option
 *              Network SSID with editable drop down suggestion
 *              Connection status header
 *              Access password type changed from "password" to "text".
 */


#include "config.h"
#include "ZtBsp.h"
#include "HtmlPage.h"
#include "HttpServer.h"
#include "BspEeprom.h"
#include "TempSensor.h"
#include <stdint.h>

/*  Server at this port with IP 192.168.4.1. */
ESP8266WebServer server(httpPort_server);
extern struct EEP_Strings EEP_String;
extern struct strms counter;
extern struct temp_val temperature;
extern uint32_t response_count;

/* Start access point with "ESP_'(mac of device)'" string */
bool start_access_point(const char *name,const char *password, int mode){
	return WiFi.softAP(name,password,wifi_ap_channel,mode);
}

void http_server_init(void){
	server.on("/", handleRoot);									
	server.begin();
}

void handle_client(void){
  
  /* handle the client at server 192.168.4.1*/
	server.handleClient();					
}

void handleRoot(void) {
#if debug
	for(char i=0;i<server.args();i++){
		Serial.println(server.arg(i));
	}
#endif
  /** Verify the access password i.e. zenatix123!@# 
   *  check the unblank field that needs to be changed.
   *  if the fields are blank then ignore the field and there won't be any changes in that specific field. 
  */
	if(server.arg(0)==Page_password){											
		if(server.arg(5)!="") strcpy(EEP_String.AP_password,server.arg(5).c_str()); 
    if(server.arg(6)!="") strcpy(EEP_String.AUTH_pass,server.arg(6).c_str()); 
		if(server.arg(1)!="") strcpy(EEP_String.Network_SSID,server.arg(1).c_str());					
		if(server.arg(2)!="") strcpy(EEP_String.Network_password,server.arg(2).c_str());				
		if(server.arg(3)!="") strcpy(EEP_String.Network_gateway,server.arg(3).c_str());					
		if(server.arg(4)=="yes") EEP_String.hide_ssid = 1;						
		else if(server.arg(4)=="no") EEP_String.hide_ssid = 0;					

    /*  Write all the objects to the eeprom with respective changes. */
		eeprom_write_obj(EEP_network_parm_start,(char*)&EEP_String,sizeof(struct EEP_Strings));
	}
	   int size = strlen(EEP_String.Network_password);
	   char pass_to_show[size];
	   bool toggle=1;int i=0;
	   for(;i<size;i++){
	       if(toggle)pass_to_show[i] = EEP_String.Network_password[i];
	       else pass_to_show[i] = '*';
	       toggle = !toggle;
	   }
	   pass_to_show[i] = 0;

    /* Page when IP is not allotted / Sensor is not connected to the WLAN */

    
     if(WiFi.localIP().toString()== "0.0.0.0"){
          /*send the html form constutted by the string. */
        	server.send(200, "text/html",page_str1+WiFi.macAddress()+page_str2+WiFi.localIP().toString()+page_str3+wifi_station_get_rssi()  	
        		                +page_str3_5+hrdwre_ver+page_str3_6+EEP_String.firm_ver
        						+page_str4+String(counter.reboot)+page_str5+String(counter.reconnect)+page_str6
        		                +String(response_count++)+page_str7+String(counter.temp_sampling)+page_str7_6+sensor+page_str7_8+String(temperature.temp_val/100.0)+page_str7_8_1 + EEP_String.Network_SSID +page_str7_8_2+ EEP_String.Network_password + page_str7_8_3+EEP_String.Network_gateway+ page_str8+EEP_String.AUTH_pass
        		                +page_str10+EEP_String.Network_SSID+page_str11+pass_to_show
        		                +page_str12+EEP_String.Network_gateway
        						+page_str14+button + page_str15);
     }
     else
     {
      
      /*Page when IP is allotted / Sensor is connected to the WLAN */
      server.send(200, "text/html",page_str1+WiFi.macAddress()+page_str2+WiFi.localIP().toString()+page_str3+wifi_station_get_rssi() 
                    +page_str3_5+hrdwre_ver+page_str3_6+EEP_String.firm_ver
            +page_str4+String(counter.reboot)+page_str5+String(counter.reconnect)+page_str6
                    +String(response_count++)+page_str7+String(counter.temp_sampling)+page_str7_6+sensor+page_str7_8+String(temperature.temp_val/100.0)+page_str7_8_1 + EEP_String.Network_SSID+page_str7_8_2+ EEP_String.Network_password + page_str7_8_3+EEP_String.Network_gateway+page_str8+EEP_String.AUTH_pass
                    +page_str10+EEP_String.Network_SSID+page_str11+pass_to_show
                    +page_str12+EEP_String.Network_gateway
            +page_str14+button + page_str16);
     }
     if(server.arg(0)==Page_password)
     {
      /* If access password is right then hard reset the system.*/
      system_restart_hard();
     }
}

