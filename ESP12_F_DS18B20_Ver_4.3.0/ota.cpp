/*
 * ota.cpp
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#include "config.h"
#include "ZtBsp.h"

ESP8266WiFiMulti WiFiMulti;					// To handle multiple client at http OTA

bool ota_access_pt(const char* network,const char* password){
	return WiFiMulti.addAP(network,password); // Add connected access point for multiple operation
}

int ota_update(String new_firm_ver, const char *host){								// disable the one second timer interrupt
#if debug
	Serial.println("Looking for ota packet");
#endif
	uint16_t time_spent = 0;					// Timeout functionality
	while((time_spent<=ota_timeout)&&(WiFiMulti.run() != WL_CONNECTED)){	// check for connection with certain time period
		time_spent++;					// Increase the time at every 1 ms
		delay(1);						// 1 ms delay
	}
	if(time_spent>=ota_timeout) {				// If time is spent and couln't get the response
		return HTTP_UPDATE_FAILED;
	}
	else {
#if debug
		Serial.println(ota_dir_path+String(hrdwre_ver)+"/ESP12_F_v"+new_firm_ver+".bin"); //print the path for requesting the binaries
#endif
		HTTPUpdateResult update = ESPhttpUpdate.update(host, httpPort_client, ota_dir_path+String(hrdwre_ver)
								+"/ESP12_F_v"+new_firm_ver+".bin"); //Else download the firmware
#if debug
		Serial.println("update code="+ String((int)update));
#endif
		return (int)update;
	}
}
