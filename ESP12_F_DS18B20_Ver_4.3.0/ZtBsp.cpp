/*
 * ZT_BSP.c
 *
 *  Created on: 02-Feb-2017
 *      Author: root
 */
#include "ZtBsp.h"
#include "config.h"

void system_led_on(void){
	pinMode(BOARD_LED,OUTPUT);				// Indication LED
	digitalWrite(BOARD_LED,0);				// Make LED ON
}

void system_led_off(void){
	digitalWrite(BOARD_LED,1);				// Make LED ON
}

void wait_ms(unsigned long ms){
	delay(ms);
}

void system_init(void){
	system_update_cpu_freq(SYS_CPU);	// CPU frequency 160 MHz
	WiFi.mode(WIFI_AP_STA);			// Set for Station and Access point both
#if debug
	Serial.begin(115200);			// begin the serial at 115200 baud rate
	Serial.println();
#endif
}

void system_restart_hard(void){
	EEPROM.end();
	delay(300);						// wait for buffer empty before restart
	pinMode(SYS_RST_PIN,OUTPUT);
	digitalWrite(SYS_RST_PIN,LOW);
}
