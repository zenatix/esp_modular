/*
 * html.h
 *
 *  Created on: 26-Jul-2016
 *      Author: ashish
 */

#ifndef HTMLPAGE_H_
#define HTMLPAGE_H_

const String page_str1		=	"<form style=\"margin-left:250px\" action=\"\" method=\"post\"><div><h1 style=\"color:42D1;font-size:80;\">zenatix</h1><h2>sensor configuration</h2><h4>MAC address = ";
        						//(mac_address)
const String page_str3 		=	"<br>RSSI value = ";
        						//(RSSI_value)
const String page_str3_5 	=	"<br>Hardware version = ";
        						//(hardware version)
const String page_str3_6 	=	"<br>Firmware version = ";
        						//(firmware version)
const String page_str2		=   "<br>STA IP = ";
        						//(Client_IP)
const String page_str4		=   "<br>Reboot count = ";
        						//(Reboot_count)
const String page_str5		=	"<br>Reconnect count = ";
        						//(reconnect_count)
const String page_str6		=	"<br>Response count = ";
								//(Response_count)
const String page_str7		=   "<br>Cycle Rate = ";
        						//(sampling_interval)
const String page_str7_6	= 	"<br>Sensor = ";
        						//(sensor_type)
const String page_str7_8    =   "<br>Temperature = ";
								// (Temperature)
const String page_str8		=	" &#8451;</h4><br><br><label for=\"apass\">Access Password </label><input style=\"margin-left:45px\" type=\"password\" name=\"apass\"";
								//AUTH_password
const String page_str9		=	"\" required><br><br><label for=\"appass\">AP password </label><input style=\"margin-left:70px\" type=\"text\" name=\"appass\" placeholder=\"";
								//AP Password
const String page_str10		=	"\"><br><br><label for=\"nssid\">Network SSID </label><input style=\"margin-left:60px\" type=\"text\" name=\"nssid\" placeholder=\"";
								//network_SSID
const String page_str11		=	"\" required><br><br><label for=\"npass\">Network Password </label><input style=\"margin-left:35px\" type=\"text\" name=\"npass\" placeholder=\"";
								//network password
const String page_str12		=	"\" required><br><br><label for=\"gtway_ip\">Gateway IP </label><input style=\"margin-left:80px\" type=\"text\" name=\"gtway_ip\" placeholder=\"";
								//Gateway IP
const String page_str13		=	"\" required><br><br><label for=\"napass\">New Access Password </label><input style=\"margin-left:10px\" type=\"text\" name=\"napass\"";
const String page_str14		=	"<br><br><br><label for=\"hssid\">Hide ssid (yes/no) </label><input style=\"margin-left:40px\" type=\"text\" name=\"hssid\"";
const String button			=	"<br><br><br><input style=\"margin-left:135px\" type=\"submit\" value=\"SUBMIT\"></div></form>";

#endif /* HTMLPAGE_H_ */
