/*
 * debug_print.h
 *
 *  Created on: 03-Feb-2017
 *      Author: root
 */

#ifndef DEBUGPRINT_H_
#define DEBUGPRINT_H_

/*
 * Function for printing the values
 * for serial debugging
 * arguments : nothing
 * returns : nothing
 */
void print_settings(void);


#endif /* DEBUGPRINT_H_ */
