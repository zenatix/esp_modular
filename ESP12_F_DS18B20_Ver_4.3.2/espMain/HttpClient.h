/**
 * @file HttpClient.h
 *
 *  Created on: 04-Feb-2017
 *      Author: root
 */

#ifndef HTTPCLIENT_H_
#define HTTPCLIENT_H_


/**
 * @brief connect to network
 * 
 * @param   network_name    SSID Name
 * @param   password        Password of AP
 * 
 * @return  status
 */
wl_status_t connect_to(const char *network_name,const char *password);

/**
 * @brief Generate and send put request.
 * 
 * @details:
 * Function generates put request and send it to the gatway.
 * Actions:
 *        -#  
 *        
 * @param   put_req     The string which needs to be send through put request.
 * @param   gateway     IP of of Network gateway.
 * @param   success     Variable to store status of the string
 * 
 * @return  server response
 */
 
String put_request(String put_req,char *gateway,int *success);

/**
 * @brief   check whether host is available or not
 * 
 * @param   host           IP of network gateway
 * @param   port           Port of gatway through which communication will be established
 * 
 * @return status
 */
int check_connection(const char* host, int port);

/**
 * @brief Function to parse server response
 * 
 * @param     extr_val    Server response     
 * @param     ret_sample  pointer to variable to store Sampling rate
 * 
 * @return    firmware version
 */
String get_response(String extr_val,uint16_t *ret_sample);

#endif /* HTTPCLIENT_H_ */
